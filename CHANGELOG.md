<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.5.3...v1.6.0) (2019-07-29)


### Bug Fixes

* **scylla_spree_threepl:** digital orders do not sync to 3PL ([5aeabfc](https://gitlab.com/5stones/scylla-spree-threepl/commit/5aeabfc))



<a name="1.5.3"></a>
## [1.5.3](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.5.2...v1.5.3) (2019-07-23)


### Bug Fixes

* **tasks.py:** key_field for toThreeOLOrderTask change to 'id' ([44bb552](https://gitlab.com/5stones/scylla-spree-threepl/commit/44bb552))



<a name="1.5.2"></a>
## [1.5.2](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.5.1...v1.5.2) (2019-03-28)


### Bug Fixes

* **ToThreePLRoutingInfo:** Fix issue with unmappable shipping methods by passing only the full metho ([6bea560](https://gitlab.com/5stones/scylla-spree-threepl/commit/6bea560))



<a name="1.5.1"></a>
## [1.5.1](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.5.0...v1.5.1) (2019-03-27)


### Bug Fixes

* **App:** Add filter to only update shipments for completed orders ([bc70dd8](https://gitlab.com/5stones/scylla-spree-threepl/commit/bc70dd8))



<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.4.3...v1.5.0) (2019-03-25)


### Features

* **App:** Make inventory sync tasks on-demand ([09affdd](https://gitlab.com/5stones/scylla-spree-threepl/commit/09affdd))



<a name="1.4.3"></a>
## [1.4.3](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.4.2...v1.4.3) (2019-03-21)


### Features

* **ToThreePLOrder:** Add poNum and earliestShipDate ([a5e696c](https://gitlab.com/5stones/scylla-spree-threepl/commit/a5e696c))



<a name="1.4.2"></a>
## [1.4.2](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.4.1...v1.4.2) (2019-03-19)


### Bug Fixes

* **ToThreePLOrderTask:** Set key_field on ToThreePLOrderTask for individual runs ([aa76cca](https://gitlab.com/5stones/scylla-spree-threepl/commit/aa76cca))



<a name="1.4.1"></a>
## [1.4.1](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.4.0...v1.4.1) (2019-03-19)


### Bug Fixes

* **App:** Fix filtration on spree-shipment to exclude deleted shipments ([97f6355](https://gitlab.com/5stones/scylla-spree-threepl/commit/97f6355))



<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.3.0...v1.4.0) (2019-03-12)


### Features

* **App:** Make tasks customer specific ([a1e298b](https://gitlab.com/5stones/scylla-spree-threepl/commit/a1e298b))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.2.3...v1.3.0) (2019-01-21)


### Features

* **ToThreePLLineItem:** Add the ability to pass aliased SKU's to 3PL if no Item exists ([7360814](https://gitlab.com/5stones/scylla-spree-threepl/commit/7360814))



<a name="1.2.3"></a>
## [1.2.3](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.2.2...v1.2.3) (2018-12-12)


### Bug Fixes

* **ToSpreeStockItem:** Fix issue with spree error on negative inventory adjustments ([cbe8dbb](https://gitlab.com/5stones/scylla-spree-threepl/commit/cbe8dbb))



<a name="1.2.2"></a>
## [1.2.2](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.2.1...v1.2.2) (2018-12-11)


### Bug Fixes

* **UpdateSpreeStockItemTask:** Fix issue with processing records one at a time ([12e553e](https://gitlab.com/5stones/scylla-spree-threepl/commit/12e553e))



<a name="1.2.1"></a>
## [1.2.1](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.2.0...v1.2.1) (2018-12-11)


### Bug Fixes

* **ToSpreeStockItem:** Do not pass backorder status ([89ff691](https://gitlab.com/5stones/scylla-spree-threepl/commit/89ff691))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.1.3...v1.2.0) (2018-12-07)


### Features

* **UpdateSpreeStockItemTask:** Adjust inventory based on allocated items not yet pushed into 3PL ([488b7a7](https://gitlab.com/5stones/scylla-spree-threepl/commit/488b7a7))



<a name="1.1.3"></a>
## [1.1.3](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.1.2...v1.1.3) (2018-12-06)


### Bug Fixes

* **LinkInventoryTask:** Fix issue with task id collisions by specifying custom task id ([1c0d2b8](https://gitlab.com/5stones/scylla-spree-threepl/commit/1c0d2b8))



<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.1.1...v1.1.2) (2018-12-06)


### Bug Fixes

* **LinkInventoryTask:** Specify key_field for single item sync's ([407b839](https://gitlab.com/5stones/scylla-spree-threepl/commit/407b839))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.1.0...v1.1.1) (2018-12-05)


### Bug Fixes

* **ToThreePLLineItem:** Update deprecated field to new name ([0dcc335](https://gitlab.com/5stones/scylla-spree-threepl/commit/0dcc335))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.0.1...v1.1.0) (2018-12-04)


### Features

* **ToThreePLShipToInfo:** Add requisite companyName field to shipTo data ([e2223c6](https://gitlab.com/5stones/scylla-spree-threepl/commit/e2223c6))
* **UpdateSpreeStockItemTask, LinkInventoryTask, ToSpreeStockItem:** Add tasks and conversions for u ([2cdcf84](https://gitlab.com/5stones/scylla-spree-threepl/commit/2cdcf84))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/5stones/scylla-spree-threepl/compare/v1.0.0...v1.0.1) (2018-11-06)


### Bug Fixes

* **requirements.txt:** Specify alpha.0 of scylla-threepl for proper install ([83f69fb](https://gitlab.com/5stones/scylla-spree-threepl/commit/83f69fb))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/5stones/scylla-spree-threepl/compare/v0.0.1...v1.0.0) (2018-10-29)



<a name="0.0.1"></a>
## [0.0.1](https://gitlab.com/5stones/scylla-spree-threepl/compare/bfa0df9...v0.0.1) (2018-10-29)


### Bug Fixes

* **setup.py:** Fix module names in setup script ([c9986e7](https://gitlab.com/5stones/scylla-spree-threepl/commit/c9986e7))


### Features

* **__init__.py:** Update module exports ([4f43f79](https://gitlab.com/5stones/scylla-spree-threepl/commit/4f43f79))
* ***/*:** Create basic module for converting Spree shipments to 3PL orders ([bfa0df9](https://gitlab.com/5stones/scylla-spree-threepl/commit/bfa0df9))
* **update-fulfilled:** Add a new task to update fulfillment status in Spree ([7cc557d](https://gitlab.com/5stones/scylla-spree-threepl/commit/7cc557d))



