from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-spree-threepl',
    packages = ['scylla_spree_threepl'],
    version = version,

    description = 'Scylla-Spree-ThreePL provides the basic utilities to integrate with the standard spree & 3PL API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-spree-threepl',
    download_url = 'https://gitlab.com/5stones/scylla-spree-threepl/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla spree',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'scylla',
        'scylla-threepl',
        'scylla-spree',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'git+https://gitlab.com/5stones/scylla-threepl.git',
        'git+https://gitlab.com/5stones/scylla-spree.git',
        'requests[security]',
    ],
)
