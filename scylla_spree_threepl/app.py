from scylla import app
from scylla import configuration
import scylla_spree as spree
import scylla_threepl as threepl
from . import conversions
from . import tasks

class App(app.App):

    def _prepare(self):

        self.spree_client = spree.SpreeRestClient()

        config = configuration.getSection('ThreePL')
        client = threepl.ThreePLRestClient(
            config['name'],
            config['url'],
            config['client_id'],
            config['client_secret'],
            config['three_pl_key']
        )
        self.threepl_client = client

        self.tasks['spree-shipment'] = tasks.ToThreePLOrderTask(
            (self.spree_client.name, 'Shipment'),
            conversions.ToThreePLOrder(config['customer_id'], self.spree_client.name),
            self.threepl_client,
            'Order',
            where=(
                "state NOT IN ['shipped', 'canceled'] "
                "AND _deleted_at IS NULL "
                "AND _parent.state = 'complete' "
                "AND selected_shipping_rate.name != 'Digital Delivery'"
            ),
            # only shipments not sync'd to threepl
            with_reflection=False
        )

        self.tasks['update-fulfilled'] = tasks.ShipSpreeShipmentsTask(
            (config.get('name'), 'Order'),
            self.spree_client,
            where=(
                "_CustomerId = {} "
                "AND _ProcessDate IS NOT NULL "
                "AND ReadOnly.IsClosed = true "
                "AND ReadOnly.Status = 1 "
                "AND in('Created')[0].state = 'ready' "
            ).format(config['customer_id']),
        )

        self.on_demand_tasks['link-inventory'] = tasks.LinkInventoryTask(
            (config.get('name'), 'StockSummary'),
            (self.spree_client.name, 'StockItem'),
            where='_CustomerId = {}'.format(config['customer_id']),
            with_reflection=False,
        )

        self.on_demand_tasks['update-inventory'] = tasks.UpdateSpreeStockItemTask(
            (config.get('name'), 'StockSummary'),
            self.spree_client,
            where='_CustomerId = {}'.format(config['customer_id']),
            conversion=conversions.ToSpreeStockItem(),
            with_reflection=True
        )
