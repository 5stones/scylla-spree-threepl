from scylla import convert
from scylla import orientdb
from pprint import pprint


class ToThreePLLineItem(convert.Conversion):
    """Convert an Spree line item to an 3PL line item"""

    def __init__(self, from_prefix):
        super(ToThreePLLineItem, self).__init__()

        self.from_prefix = from_prefix
        self.conversion_map.update({
            'itemIdentifier': (
                'variant_id',
                lambda variant_id: self._get_item_identifier_by_variant_id(variant_id, self.from_prefix),
            ),
            'qty': ('quantity', ),
            'savedElements': (self._get_saved_elements, ),
        })

    @staticmethod
    def _get_item_identifier_by_variant_id(id, from_prefix):
        sku = orientdb.lookup('{}Variant'.format(from_prefix), 'id', id, 'sku')
        item_identifier = { 'sku': sku }

        try:
            item_id = orientdb.lookup('ThreePLItem', 'Sku', sku, 'ItemId')
            item_identifier['id'] = item_id
        except orientdb.OrientDbError:
            print('Unable to locate ThreePLItem with sku {}. Attempting to send as an alias.'.format(sku))

        return item_identifier

    @staticmethod
    def _get_saved_elements(item):
        return []


class ToThreePLOrderContactInfo(convert.Conversion):
    def __init__(self):
        super(ToThreePLOrderContactInfo, self).__init__()

        self.conversion_map.update({
            'companyName': '',
            'name': ('full_name', ),
            'title': '',
            'address1': ('address1', ),
            'address2': (self._get_address2, ),
            'city': ('city', ),
            'state': ('state_text', ),
            'zip': ('zipcode', ),
            'country': ('country', 'iso', ),
            'phoneNumber': ('phone', ),
            'fax': '',
            'emailAddress': '',
            'dept': '',
            'code': '',
            'addressStatus': 0,
        })

    @staticmethod
    def _get_address2(address):
        if 'address2' in address and address['address2'] is not None:
            return address['address2']
        else:
            return ''

class ToThreePLShipToInfo(ToThreePLOrderContactInfo):
    def __init__(self):
        super(ToThreePLShipToInfo, self).__init__()

        self.conversion_map.update({
            'companyName': ('full_name', ),
        })


class ToThreePLRoutingInfo(convert.Conversion):

    def __init__(self):
        super(ToThreePLRoutingInfo, self).__init__()

        self.conversion_map.update({
            'isCod': False,
            'isInsurance': False,
            'requiresDeliveryConf': False,
            'requiresReturnReceipt': False,
            'mode': ('shipping_method_code', ),
        })


class ToThreePLOrder(convert.Conversion):
    """Convert an Spree Shipment to a 3PL order"""

    from_type = 'Shipment'
    to_type = 'Order'

    def __init__(self, customer_id, from_prefix):
        super(ToThreePLOrder, self).__init__()

        line_conversion = ToThreePLLineItem(from_prefix)

        self.conversion_map.update({
            'customerIdentifier': { 'id': customer_id },
            'facilityIdentifier': { 'id': 1 },
            # 'warehouseTransactionSourceEnum': (),
            # 'transactionEntryTypeEnum': (),
            # 'importChannelIdentifier': (),
            # 'importModuleId': (),
            # 'exportModuleIds': (),
            # 'deferNotification': (),
            'referenceNum': ('number', ),
            # 'description': (),
            'poNum': ('order_id', ),
            'externalId': ('number', ),
            'earliestShipDate': ('_parent', 'completed_at', ),
            # 'shipCancelDate': (),
            # 'notes': (),
            # 'numUnits1': (),
            # 'unit1Identifier': (),
            # 'numUnits2': (),
            # 'unit2Identifier': (),
            # 'totalWeight': (),
            # 'totalVolume': (),
            # 'billingCode': (),
            # 'asnNumber': (),
            # 'upsServiceOptionCharge': (),
            # 'upsTransportationCharge': (),
            # 'addFreightToCod': (),
            # 'upsIsResidential': (),
            # 'exportChannelIdentifier': (),
            # 'routePickupDate': (),
            # 'shippingNotes': (),
            # 'masterBillOfLadingId': (),
            # 'invoiceNumber': (),
            # 'fulfillInvInfo': (),
            'routingInfo': ('selected_shipping_rate', ToThreePLRoutingInfo(), ),
            'shipTo': ('_parent', 'ship_address', ToThreePLShipToInfo(), ),
            'soldTo': ('_parent', 'bill_address', ToThreePLOrderContactInfo(), ),
            'billTo': ('_parent', 'bill_address', ToThreePLOrderContactInfo(), ),
            'savedElements': (self._get_saved_elements, ),
            'orderItems': (
                'manifest',
                lambda items: [line_conversion(item) for item in items],
            ),
        })

    @staticmethod
    def _get_saved_elements(order):
        return []


class ToSpreeStockItem(convert.Conversion):
    """Convert an 3PL StockSummary to a Spree StockItem"""

    from_type = 'StockSummary'
    to_type = 'StockItem'

    def __init__(self):
        super(ToSpreeStockItem, self).__init__()

        self.conversion_map.update({
            'id': ('_linked_to', 'id', ),
            'count_on_hand': (self._get_count_on_hand, ),
            'stock_location_id': ('_linked_to', 'stock_location_id', ),
            # 'variant_id': ('_linked_to', 'variant_id', ),
            'force': True,
        })

    def __call__(self, obj, on_hand_adjustment=0):
        data = self.convert(obj)

        if on_hand_adjustment > 0:
            data['count_on_hand'] = data.get('count_on_hand') - on_hand_adjustment

        # spree doesn't like negative/overallocated inventory
        if data.get('count_on_hand', 0) < 0:
            data['count_on_hand'] = 0

        return data

    @staticmethod
    def _get_count_on_hand(obj):
        return obj.get('_available', 0)
